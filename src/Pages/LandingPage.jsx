import { Box, Container } from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useEffect } from "react";
import Cards from "../Components/Cards";
import Lists from "../Components/Lists";

const LandingPage = () => {
    const [datas, setDatas] = useState()

    const handleFetch = async () => {
        const data = await axios.get('http://localhost:3004/postgenerated')
        setDatas(data.data)
    }

    useEffect(() => {
        handleFetch()
    }, []);

    console.log(datas)

    return (
        <Container sx={{ display: { md: "flex" } }}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap', maxWidth: { md: "75%", sm: "100% " }, margin: '5px solid pink' }} >
                {datas?.map((data) => {
                    return (
                        <Cards key={data.post_id} title={data.title} date={data.datePost} desc={data.description} img={data.img} />
                    )
                })}
            </Box>
            <Box sx={{ display: { md: 'block', sm: 'none' } }}>
                <Lists />
            </Box>

        </Container>
    );
};

export default LandingPage;
